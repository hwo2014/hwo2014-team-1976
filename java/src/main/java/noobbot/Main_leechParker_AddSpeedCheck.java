package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import noobbot.Game.TrackPiece;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main_leechParker_AddSpeedCheck {
	
	private float enginePower = 1;
	private float trackFriction = 1;
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main_leechParker_AddSpeedCheck(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    List<TrackPiece> pieces;
    LinkedTreeMap<Integer, TrackPiece> piecesT;
    
    public Main_leechParker_AddSpeedCheck(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        
        send(join);

        
        double last = 0;
        Game raceInfo= null;
        
        int currentPCIdx = 0;
        TrackPiece currentPC = null; //= pieces.get(currentPCIdx);
        TrackPiece nextPC = null;  //= pieces.get(currentPCIdx+1);
        TrackPiece nextnextPC = null; //= pieces.get(currentPCIdx+2);
         
        CarPosition prevPos=null;
        CarPosition currentPos=null;
        double speed=0;
        boolean isSwitched=false;
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	String str = msgFromServer.data.toString();
            	
            	currentPos = gson.fromJson(line, CarPosition.class);
            	if (prevPos==null)
            	{
            		prevPos = currentPos;
            	}
            	if (currentPos.data[0].piecePosition.pieceIndex == prevPos.data[0].piecePosition.pieceIndex)
            	{
            		speed = currentPos.data[0].piecePosition.inPieceDistance - prevPos.data[0].piecePosition.inPieceDistance;
            		System.out.println("In Same Pieces: " + currentPos.data[0].piecePosition.inPieceDistance + " " + prevPos.data[0].piecePosition.inPieceDistance);
            	}
            	else
            	{	
            		int prevIdx = (int)prevPos.data[0].piecePosition.pieceIndex;
            		currentPCIdx = (int)currentPos.data[0].piecePosition.pieceIndex;
            		System.out.println("In Different Pieces: " + currentPos.data[0].piecePosition.pieceIndex + " Prev?: " + prevIdx);
            		double lengthPrev = pieces.get(prevIdx).length;  
            		speed = currentPos.data[0].piecePosition.inPieceDistance +
            				  (lengthPrev - prevPos.data[0].piecePosition.inPieceDistance);
            		
            		
            		if (currentPCIdx==pieces.size()-1)
            		{
            			currentPCIdx = 0;
            			currentPC = pieces.get(currentPCIdx);
            		}
            		else
            		{
            			currentPC = pieces.get(currentPCIdx);
            		}
            		System.out.println("Currnent IDX: " + currentPCIdx);
            		
            		if (currentPCIdx+1>=pieces.size()-1)
            		{	
            			System.out.println("Next IDX: " + 0);
            			nextPC = pieces.get(0);
            		}
            		else
            		{
            			System.out.println("Next IDX: " + (currentPCIdx+1));
            			nextPC  = pieces.get(currentPCIdx+1);
            		}
            			
                    
            		if (currentPCIdx+2>=pieces.size()-1)
            		{	
            			System.out.println("NextNext IDX: " + 0);
            			nextnextPC = pieces.get(0);
            		}
            		else
            		{
            			System.out.println("NextNext IDX: " + (currentPCIdx+2));
            			nextnextPC  = pieces.get(currentPCIdx+2);
            		}
            	}
            	prevPos = currentPos;
            	
            	System.out.println("CurIdx: " + currentPCIdx + " CurrentPC " + currentPC + "Is Bend " +  currentPC.isBend() + "Speed : " + speed );
            	System.out.println("Current : " + currentPos.data[0].piecePosition.pieceIndex + " Angle:" + currentPos.data[0].angle);
            	//System.out.println("IsSwitched: " + isSwitched);
            	
        		if (isSwitched && currentPC.isSwitch)
        		{
        			//Reset the switch
        			isSwitched = false;
        		}
        		if (nextPC.isSwitch)
        		{
        			System.out.println("Next is switch");
        		}
        		
        		
            	if (!isSwitched && nextPC.isSwitch &&  (nextnextPC.isBend()) )
            	{
            		TrackPiece bendPC = nextnextPC;
            		System.out.println("there is a switch, what is the angeL? " + bendPC.angle);
            		if (bendPC.angle < 0.0)
            		{
//                			System.out.println("send switch left");
            			System.out.println("Switch LEFT. ");
            			isSwitched = true;
            			send(new SwitchLane("Left"));
            		}
            		else if (bendPC.angle > 0.0)
            		{
//                			System.out.println("send switch right");
            			System.out.println("Switch RIGHT. ");
            			isSwitched = true;            			
            			send(new SwitchLane("Right"));
            		}
            		else
            		{
            			send(new Throttle(0.72));
            		}
            	}
            	else if (currentPC.isSwitch && !nextPC.isBend())
            	{
            		send(new Throttle(0.7));
            	}
            	else if (currentPC.isBend())
            	{
            		//send(new Throttle(0.45));
            		if (speed < 5.6)
            		{
            			send(new Throttle(0.77));
            		}
            		if (speed < 6)
            		{
            			send(new Throttle(0.66));
            		}
            		else if (speed < 6.5)
            		{
            			send(new Throttle(0.54));
            		}
            		else
            		{
            			send(new Throttle(0.45));
            		}
            	}
            	else if (nextPC.isBend())
            	{
            		System.out.println("Ready to Slowing down for bend? ");
            		if (  currentPos.data[0].piecePosition.inPieceDistance / currentPC.length > 0.5  )
            		{
            			System.out.println("Yes. Slow down.. ");
            			if (speed > 8)
            			{
            				System.out.println("Going tooooooo fast.....");
            				send(new Throttle(0.45));
            			}
            			if (speed < 7)
            			{
            				System.out.println("Going tooooooo fast.....");
            				send(new Throttle(0.56));
            			}
            			else
            			{
            				send(new Throttle(0.5));
            			}
            		}
            		else
            		{
            			System.out.println("Next is bend, but still got elephants infront.");
            			send(new Throttle(0.8));
            		}           		
            	}
            	else
            	{              
            		//Normal?? Straight?
            		send(new Throttle(0.76));
            	}
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	//System.out.println(line);
            	raceInfo =  gson.fromJson(line, Game.class);
            	pieces = raceInfo.data.race.track.pieces;
            	initPieces();
            	currentPC = pieces.get(currentPCIdx);
                nextPC  = pieces.get(currentPCIdx+1);
                nextnextPC = pieces.get(currentPCIdx+2);
                //System.out.println("Race init >>> " + raceInfo);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
            	
                System.out.println("Race start");
            } else {
            	System.out.println("Others>>>"  + msgFromServer.msgType + ">>>> "  + msgFromServer.data);
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
    	System.out.println(msg.toJson());
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private void initPieces()
    {
    	for(TrackPiece pcs : pieces)
    	{
    		if (pcs.isBend() && pcs.length == 0.0)
    		{
    			pcs.length = Math.abs((float)(2 * 3.14 * pcs.radius * pcs.angle/360.0));
    		}
    	}
    	System.out.println("Pieces " + pieces);
    }

}