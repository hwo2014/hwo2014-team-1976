package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import noobbot.Game.TrackLane;
import noobbot.Game.TrackPiece;

import com.google.gson.Gson;

public class MainHM2 {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new MainHM2(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public MainHM2(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        Game raceInfo= null;
        double currentLane;
        int totalLane = 0;
        int totalPiece = 0;
        int inspectedIndex = -1;
        double previousInPieceDistance=0.0;
        double currentInPieceDistance;
        double previousLen=0.0;
        boolean sendOnce = true;
        double previousSpeed=0.0;
        double currentSpeed=0.0;
        double desiredSpeed=0.5;
        double enginePower=0.0;
        
        List<TrackLane>trackLanes = null;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	final CarPosition  carPosition = gson.fromJson(line, CarPosition.class);
            	int idx = (int)carPosition.data[0].piecePosition.pieceIndex;
            	currentLane = carPosition.data[0].piecePosition.lane.startLaneIndex;
            	currentInPieceDistance = carPosition.data[0].piecePosition.inPieceDistance;
            	TrackPiece pc =  raceInfo.data.race.track.pieces.get(idx);	
            	
            	int left = totalPiece - idx;
            	int len = 3;
            	if(left < 3){
            		len = left;
            	}
            		
            	boolean sendThrottle = true;
            	double speed;
            	
            	previousSpeed = currentSpeed;
            	
            	if(inspectedIndex != idx){
            		inspectedIndex = idx;
            		
	            	boolean cross = false;
	            	int currentIndex = 0;
	            	for(int index=0;index<len;index++){
	            		Game.TrackPiece pc1 =  raceInfo.data.race.track.pieces.get(currentIndex=idx+index);
	            		if(pc1.isSwitch){
	            			cross = true;
	            			index = len;
	            		}
	            	}
	            	//System.out.println(cross+":"+idx+":"+currentLane+":"+totalLane);
	            	
	            	if(cross){
	            		sendThrottle=!determineLaneChanging2(raceInfo.data.race.track.pieces,currentIndex,currentLane,totalLane-1);
	            	}
	            	//System.out.println("changed index=>"+previousLen+":"+previousInPieceDistance+":"+currentInPieceDistance);
	            	// need to subtract previous length from previous inPieceDistance and add to current inPieceDistance
	                speed = previousLen - previousInPieceDistance + currentInPieceDistance;
	            
            	}
            	// normal calculation
            	else{
            		speed = currentInPieceDistance - previousInPieceDistance;
            	}
            	
            	currentSpeed = speed;
            	
            	if(sendThrottle){
            		String str = msgFromServer.data.toString();
            		if (str.contains("pieceIndex=35.0")  || str.contains("pieceIndex=36.0") || str.contains("pieceIndex=37.0") || str.contains("pieceIndex=38.0") || str.contains("pieceIndex=39.0"))
                	{	
            			desiredSpeed = 0.70;
                	}
            		else{
            			desiredSpeed = 0.60;
            		}
            		send(new Throttle(desiredSpeed));
//            		String str = msgFromServer.data.toString();
//            		if (str.contains("pieceIndex=35.0")  || str.contains("pieceIndex=36.0") || str.contains("pieceIndex=37.0") || str.contains("pieceIndex=38.0") || str.contains("pieceIndex=39.0"))
//                	{	
//            			send(new Throttle(1.0));
//                	}
//            		else if(str.contains("pieceIndex=0.0")){
//            			send(new Throttle(0.725));
//            		}
//            		else if(str.contains("pieceIndex=1.0")){
//            			send(new Throttle(0.735));            			
//            		}
//            		else if(str.contains("pieceIndex=2.0")){
//            			send(new Throttle(0.655));            			
//            		}
//            		else if(str.contains("pieceIndex=3.0")){
//                		send(new Throttle(0.6));
//                	}
//            		else if(str.contains("pieceIndex=4.0")){
//                		send(new Throttle(0.2));
//                	}
//                	else if(str.contains("pieceIndex=5.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=6.0")){
//                		send(new Throttle(0.7));
//                	}
//            		else if(str.contains("pieceIndex=8.0")){
//                		send(new Throttle(0.93));
//                	}
//                	else if(str.contains("pieceIndex=14.0")){
//                		send(new Throttle(0.3));
//                	}
//            		else if(str.contains("pieceIndex=15.0")){
//                		send(new Throttle(0.51));
//                	}
//            		else if(str.contains("pieceIndex=16.0")){
//                		send(new Throttle(0.697));
//                	}
//            		else if(str.contains("pieceIndex=19.0")){
//                		send(new Throttle(0.5));
//                	}
//            		else if(str.contains("pieceIndex=20.0")){
//                		send(new Throttle(0.51));
//                	}
//            		else if(str.contains("pieceIndex=21.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=22.0")){
//                		send(new Throttle(0.65));
//                	}
//            		else if(str.contains("pieceIndex=24.0")){
//                		send(new Throttle(0.85));
//                	}
//            		else if(str.contains("pieceIndex=26.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=28.0")){
//                		send(new Throttle(0.78));
//                	}
//            		else if(str.contains("pieceIndex=30.0")){
//                		send(new Throttle(0.7));
//                	}
//            		else if(str.contains("pieceIndex=31.0")){
//                		send(new Throttle(0.4));
//                	}
//            		else if(str.contains("pieceIndex=32.0")){
//                		send(new Throttle(0.5));
//                	}
//            		else
//                	{
//                		send(new Throttle(0.752));
//                	}
            		
            		//7.77
//            		String str = msgFromServer.data.toString();
//            		if (str.contains("pieceIndex=35.0")  || str.contains("pieceIndex=36.0") || str.contains("pieceIndex=37.0") || str.contains("pieceIndex=38.0") || str.contains("pieceIndex=39.0"))
//                	{	
//            			send(new Throttle(1.0));
//                	}
//            		else if(str.contains("pieceIndex=0.0")){
//            			send(new Throttle(0.725));
//            		}
//            		else if(str.contains("pieceIndex=1.0")){
//            			send(new Throttle(0.735));            			
//            		}
//            		else if(str.contains("pieceIndex=2.0")){
//            			send(new Throttle(0.655));            			
//            		}
//            		else if(str.contains("pieceIndex=3.0")){
//                		send(new Throttle(0.6));
//                	}
//            		else if(str.contains("pieceIndex=4.0")){
//                		send(new Throttle(0.2));
//                	}
//                	else if(str.contains("pieceIndex=5.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=6.0")){
//                		send(new Throttle(0.7));
//                	}
//            		else if(str.contains("pieceIndex=8.0")){
//                		send(new Throttle(0.93));
//                	}
//                	else if(str.contains("pieceIndex=14.0")){
//                		send(new Throttle(0.3));
//                	}
//            		else if(str.contains("pieceIndex=15.0")){
//                		send(new Throttle(0.51));
//                	}
//            		else if(str.contains("pieceIndex=16.0")){
//                		send(new Throttle(0.697));
//                	}
//            		else if(str.contains("pieceIndex=19.0")){
//                		send(new Throttle(0.5));
//                	}
//            		else if(str.contains("pieceIndex=20.0")){
//                		send(new Throttle(0.51));
//                	}
//            		else if(str.contains("pieceIndex=21.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=22.0")){
//                		send(new Throttle(0.65));
//                	}
//            		else if(str.contains("pieceIndex=24.0")){
//                		send(new Throttle(0.85));
//                	}
//            		else if(str.contains("pieceIndex=26.0")){
//                		send(new Throttle(0.55));
//                	}
//            		else if(str.contains("pieceIndex=27.0")){
//                		send(new Throttle(0.755));
//                	}
//            		else if(str.contains("pieceIndex=28.0")){
//                		send(new Throttle(0.78));
//                	}
////            		else if(str.contains("pieceIndex=29.0")){
////                		send(new Throttle(0.7));
////                	}
//            		else if(str.contains("pieceIndex=30.0")){
//                		send(new Throttle(0.7));
//                	}
//            		else if(str.contains("pieceIndex=31.0")){
//                		send(new Throttle(0.4));
//                	}
//            		else if(str.contains("pieceIndex=32.0")){
//                		send(new Throttle(0.5));
//                	}
//            		else
//                	{
//                		send(new Throttle(0.752));
//                	}
            		//send(new Throttle(0.65));
            	}
            	
            	// Length to be used next round
            	if(pc.isBend()){
            		// note using - instead of + because it is the distance the lane is away from center
            		// lane 0 is -10 because it need to reduce 10 to reach center
            		// lane 1 is 10 because it need to add 10 to reach center 
            		// so when calculating the new radius we need to use -
            		
            		// turn left = +
            		// turn right = -
            		double radiusOffset = trackLanes.get((int)currentLane).distanceFromCenter;
            		double angle = pc.angle;
            		// turn right 
            		if(angle > 0){
            			radiusOffset = radiusOffset * -1;
            		}
            		// turn left
            		else{
            			angle = angle * -1;
            		}
            		previousLen = 2.0*22.0/7.0*(pc.radius+radiusOffset)*angle/360.0;
            		System.out.println("bend....=>"+Math.abs(pc.angle)+":"+pc.angle+":"+pc.radius+":"+(pc.radius+radiusOffset)+":"+previousLen);
            	}
            	else{
            		previousLen = pc.length;
            	}
            	
            	previousInPieceDistance = currentInPieceDistance;
            	if(enginePower == 0.0){
            		if(speed>0.0){
            			enginePower = desiredSpeed / speed;
            			System.out.println("enginePower:"+enginePower);
            		}
                	System.out.println("Speed:"+speed/10+", Acceleration:"+(currentSpeed-previousSpeed)+" ,Car pos" + carPosition + pc.isBend());
            	}
            	else{
            		System.out.println("Speed:"+speed/10+", Acceleration:"+(currentSpeed-previousSpeed)+", My computed Acceleration:"+((desiredSpeed-(speed/10))/enginePower)+" ,Car pos" + carPosition + pc.isBend());
            	}
            	
            	//if (pc.isBend())
            	//{
            		
            	//}
            	//else
            	//{
            	//	send(new Throttle(1.0));
            	//}
            	/**
            	if (str.indexOf("pieceIndex=0.0")>-1 || str.indexOf("pieceIndex=1.0")>-1 || str.indexOf("pieceIndex=2.0")>-1 || str.indexOf("pieceIndex=9.0")>-1)
            	{	
            		send(new Throttle(0.795));            	
            	}
            	if (str.indexOf("pieceIndex=35.0")>-1 || str.indexOf("pieceIndex=36.0")>-1 || str.indexOf("pieceIndex=37.0")>-1 )
            	{	
            		send(new Throttle(1.0));
            	}
            	else            	
            	{
            		send(new Throttle(0.644));
            	}*/
                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	//System.out.println(line);
            	raceInfo =  gson.fromJson(line, Game.class);
            	totalLane = raceInfo.data.race.track.lanes.size();
            	totalPiece = raceInfo.data.race.track.pieces.size();
            	trackLanes = raceInfo.data.race.track.lanes;
                System.out.println("Race init >>> " + raceInfo);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
            	System.out.println("Race start");
            } else {
            	System.out.println("Others>>>"  + msgFromServer.msgType + ">>>> "  + msgFromServer.data);
                send(new Ping());
            }
        }
    }
    
    private boolean determineLaneChanging(List<TrackPiece> pieces,int nextPiece,double currentLane,int totalLane){
    	System.out.println("nextPiece"+nextPiece);
    	// find the next bent
    	int counter = nextPiece;
    	int end = pieces.size();
    	boolean changed = false;
    	while(counter<end){
    		TrackPiece piece = pieces.get(counter);
    		if(piece.isBend()){
    			System.out.println("piece.angle:"+piece.angle+":"+counter+":"+currentLane);
    			// if angle is positive mean right turn , so keep to right
    			if(piece.angle > 0 && currentLane<totalLane){
    				changed = true;
    				send(new SwitchLane("Right"));
    				System.out.println("Swith to right");
    			}
    	    	// if angle is negative mean left turn , so keep left
    			else if(piece.angle < 0 && currentLane>0){
    				changed = true;
    				send(new SwitchLane("Left"));
    				System.out.println("Swith to left");
    			}
    	    	// if already at right most or left most skip
    			else{
    				System.out.println("No swithing is required");
    			}
    			counter = end;
    		}	
    		else
    			counter++;
    	}
    	return changed;
    	
    }
    
    private boolean determineLaneChanging2(List<TrackPiece> pieces,int nextPiece,double currentLane,int totalLane){
    	System.out.println("nextPiece"+nextPiece);
    	// find the next bent
    	int counter = nextPiece;
    	int end = pieces.size();
    	boolean changed = false;
    	double left=0.0,right=0.0;
    	
//    	if(counter<end){
//    		TrackPiece piece = pieces.get(counter);
//    		if(piece.isBend()){
//        		System.out.println("piece.angle:"+piece.angle+":"+counter+":"+currentLane);
//    			// if angle is positive mean right turn so increase right counter by one
//    			if(piece.angle > 0){
//    				right = right + piece.angle;
//    			}
//    	    	// if angle is negative mean left turn so increase left counter by one
//    			else if(piece.angle < 0){
//    				// minus as it is negative so end up as plus
//    				left = left - piece.angle;
//    			}   
//    		}
//    		counter++;
//    	}
//    	
//    	// keep going until find next cross/switch or the end
//    	while(counter<end){
//    		TrackPiece piece = pieces.get(counter);
//    		if(piece.isBend()){
//        		System.out.println("piece.angle:"+piece.angle+":"+counter+":"+currentLane);
//    			// if angle is positive mean right turn so increase right counter by one
//    			if(piece.angle > 0){
//    				right = right + piece.angle;
//    			}
//    	    	// if angle is negative mean left turn so increase left counter by one
//    			else if(piece.angle < 0){
//    				// minus as it is negative so end up as plus
//    				left = left - piece.angle;
//    			}   
//    		}
//    		
//    		// Next switch found
//    		if(piece.isSwitch){
//    			counter = end;
//    		}
//    		else{
//    			counter++;
//    		}
//    	}
    	
    	boolean next;
    	TrackPiece piece = pieces.get(counter);
    	do{
    		if(piece.isBend()){
        		System.out.println("piece.angle:"+piece.angle+":"+counter+":"+currentLane);
    			// if angle is positive mean right turn so increase right counter by one
    			if(piece.angle > 0){
    				right = right + piece.angle;
    			}
    	    	// if angle is negative mean left turn so increase left counter by one
    			else if(piece.angle < 0){
    				// minus as it is negative so end up as plus
    				left = left - piece.angle;
    			}   
    		}
    		counter++;
    		if(counter < end){
    			piece = pieces.get(counter);
    			next = !piece.isSwitch;
    		}
    		else{
    			next = false;
    		}
    	}
    	while(next);
    	System.out.println("left vs right => "+left+":"+right);
    	// keep right since there is more right turn and we are not at the most right lane
    	if(left < right && currentLane<totalLane){
			changed = true;
			send(new SwitchLane("Right"));
			System.out.println("Swith to right");
		}
	    // keep left since there is more left turn and we are not at the most left lane
		else if(right < left && currentLane>0){
			changed = true;
			send(new SwitchLane("Left"));
			System.out.println("Swith to left");
		}
    	// Either there is no bend or switching lane does not matter as there is equal amount of right and left turn.
		else if(left == right){
			// test for trackout if trackout is true
			// for trackout right, switch to left
			// for tackout left, switch to right
			// to determine angle of previous piece
			// determine if the switch piece is a bend
			piece = pieces.get(nextPiece);
    		// determine the turn point of 
    		if(piece.radius > 0){
    			changed = true;
    			send(new SwitchLane("Left"));
    			System.out.println("Swith to left");
    		}
    		else if(piece.radius < 0){
    			changed = true;
    			send(new SwitchLane("Right"));
    			System.out.println("Swith to right");
    		}
    		else{
    			System.out.println("No swithing is required");
    		}
		}
    	// No switching is required. Already at right lane
		else{
			System.out.println("No swithing is required");
		}
    	return changed;
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    
    // determine which lane we are at
}