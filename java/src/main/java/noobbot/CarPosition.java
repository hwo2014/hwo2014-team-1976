package noobbot;

public class CarPosition {
	
   CarPositionData[]   data;

   public String toString()  
   {  
          return data[0].toString();
   }
	
   class CarPositionData  {
	    
	    Id id;
	    double angle;
	    PiecePosition piecePosition;
	    

	    @Override 
	    public String toString()
	    {
		return "Id: " + id + "Angle: " + angle +  " piecePosition: " + piecePosition;
	    }
	}

	class Id {
	    String name="";
	    String color="";

	    @Override 
	    public String toString()
	    {
		return "Name : " + name + " Color: " + color;
	    }
	}

	class PiecePosition {
	     double pieceIndex;
	     double inPieceDistance;
	     Lane lane;
	     float lap;
	    
	    @Override 
	    public String toString()
	    {
		return "pieceIndex " + pieceIndex + " inPieceDistance " + inPieceDistance + " lane: " + lane + "Lap: " + lap;
	    }
	}

	class Lane {
	    double startLaneIndex;
	    double endLaneIndex;

	    @Override 
	    public String toString()
	    {
		return "startLaneIndex: "+ startLaneIndex + " endLaneIndex: " + endLaneIndex;
	    }
	}
}
