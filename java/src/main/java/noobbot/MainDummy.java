package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import noobbot.Game.TrackPiece;

import com.google.gson.Gson;

public class MainDummy {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new MainDummy(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public MainDummy(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        
        double last = 0;
        Game raceInfo= null;
        double currentLane;
        int totalLane = 0;
        int totalPiece = 0;
        int inspectedIndex = -1;
        float speed = 1.0f;
        List<TrackPiece> trackPieces = new ArrayList<TrackPiece>();
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	String str = msgFromServer.data.toString();
            	
            	final CarPosition  carPosition = gson.fromJson(line, CarPosition.class);
            	int idx = (int)carPosition.data[0].piecePosition.pieceIndex;
            	Game.TrackPiece pc =  raceInfo.data.race.track.pieces.get(idx);	
            	int left = totalPiece - idx;
            	int len = 3;
            	if(left < 3){
            		len = left;
            	}
            	boolean sendThrottle = true;
            	if(inspectedIndex != idx){
            		inspectedIndex = idx;
            		
	            	int currentIndex = 0;
	            	
	            	currentLane = carPosition.data[0].piecePosition.lane.startLaneIndex;
	            	System.out.println(idx+":"+currentLane+":"+totalLane);

	            	for(int index=0;index<len;index++){
	            		Game.TrackPiece pc1 = trackPieces.get(currentIndex=idx+index);
	            		if(pc1.isSwitch){
	            			sendThrottle=!determineLaneChanging(trackPieces,currentIndex,currentLane,totalLane-1);
	            			index = len;
	            		}
	            	}
	            	        	
	            	len = 2;
	            	if(len < 2){
	            		len = left;
	            	}
	            	
	            	for(int index=0;index<len;index++){
            			speed = 1.0f;
	            		TrackPiece pc1 = trackPieces.get(index);
	            		if(pc.isBend()){
	            			speed = 0.65f;
	            			index = len;
	            		}
	            	}
            	}
            	if(sendThrottle){
            		send(new Throttle(speed));
            	}
            	System.out.println("Car pos" + carPosition + pc.isBend()+",speed:"+speed);
            	
            	//if (pc.isBend())
            	//{
            		
            	//}
            	//else
            	//{
            	//	send(new Throttle(1.0));
            	//}
            	/**
            	if (str.indexOf("pieceIndex=0.0")>-1 || str.indexOf("pieceIndex=1.0")>-1 || str.indexOf("pieceIndex=2.0")>-1 || str.indexOf("pieceIndex=9.0")>-1)
            	{	
            		send(new Throttle(0.795));            	
            	}
            	if (str.indexOf("pieceIndex=35.0")>-1 || str.indexOf("pieceIndex=36.0")>-1 || str.indexOf("pieceIndex=37.0")>-1 )
            	{	
            		send(new Throttle(1.0));
            	}
            	else            	
            	{
            		send(new Throttle(0.644));
            	}*/
                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	//System.out.println(line);
            	raceInfo =  gson.fromJson(line, Game.class);
            	totalLane = raceInfo.data.race.track.lanes.size();
            	totalPiece = raceInfo.data.race.track.pieces.size();
            	trackPieces = raceInfo.data.race.track.pieces;
                System.out.println("Race init >>> " + raceInfo);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
            	System.out.println("Race start");
            } else {
            	System.out.println("Others>>>"  + msgFromServer.msgType + ">>>> "  + msgFromServer.data);
                send(new Ping());
            }
        }
    }
    
    private boolean determineLaneChanging(List<TrackPiece> pieces,int nextPiece,double currentLane,int totalLane){
    	System.out.println("nextPiece"+nextPiece);
    	// find the next bent
    	int counter = nextPiece;
    	int end = pieces.size();
    	boolean changed = false;
    	while(counter<end){
    		TrackPiece piece = pieces.get(counter);
    		if(piece.isBend()){
    			System.out.println("piece.angle:"+piece.angle+":"+counter+":"+currentLane);
    			// if angle is positive mean right turn , so keep to right
    			if(piece.angle > 0 && currentLane<totalLane){
    				changed = true;
    				send(new SwitchLane("Right"));
    				System.out.println("Swith to right");
    			}
    	    	// if angle is negative mean left turn , so keep left
    			else if(piece.angle < 0 && currentLane>0){
    				changed = true;
    				send(new SwitchLane("Left"));
    				System.out.println("Swith to left");
    			}
    	    	// if already at right most or left most skip
    			else{
    				System.out.println("No swithing is required");
    			}
    			counter = end;
    		}	
    		else
    			counter++;
    	}
    	return changed;
    	
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    
    // determine which lane we are at
}