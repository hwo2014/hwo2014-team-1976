package noobbot;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Game {
	
	GameData data;
	
	public String toString()  
    {  
          return data.toString();
	}
	
	class GameData
	{
		Race race;	
		//public String gameId;
		
		public String toString()
		{
			return "Game Data:  Race" + race ;
					//" Pieces: " + pieces.length + " Lanes: " + lanes.length + " First Pc: " + pieces[0] + " First Lane: " + lanes[0];				
		}
	}
	
	class Race{
		Track track;
	
		public String toString()
		{
			return "Tracks: " + track;
						
		}
	}
	
	
	class Track{
		String id;
		String name;
		ArrayList<TrackPiece> pieces;
		ArrayList<TrackLane> lanes;
		
		public String toString()
		{
			return "Track Info:  " + id + "\n Pieces : " + pieces.toString() + "\n Lanes : " + lanes.toString(); 
		}
	}
		
	class TrackPiece
	{
		double length;
		double radius;
		double angle;
		
		public boolean isBend()
		{
			return radius>0;
		}
		
		@SerializedName("switch")
		public boolean isSwitch;
		
		public String toString()
		{
			return "\nTrack Piece Info: Len: " + length + " Radius: " + radius + " Angle: " + angle + " isSwitchPc: " + isSwitch + " IsBend: " + isBend(); 
		}
	}
	
	class TrackLane
	{
		double distanceFromCenter;
		double index;
		
		public String toString()
		{
			return "Track Lane Info: Len: " + distanceFromCenter + " Index: " + index; 
		}
	}
	
	class Car{
		
		Id id;
		CarDimension dimensions;
		
		class Id
		{
			String name;
			String color;
			
			public String toString()
			{
				return "Car ID>  Name: " + name + " Color: " + color; 
			}
		}
		
		
		class CarDimension{
			float length;
			float width;
			float guideFlagPosition;
			
			public String toString()
			{
				return "CarDimension>  Len: " + length + " Width: " + width + " GuideFlag: " + guideFlagPosition;  
			}
		}
	}
}


/*
  	{"msgType":"gameInit",
		"data":	{
			"race":{
				"track":{
					"id":"keimola","name":"Keimola",
					"pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],
					"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"botZZZ","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],
					"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}
				}
			},
			"gameId":"c9552f5c-580f-4d58-a28a-893322396082"
	}
*/
